% Scripting introduction 
% Szymon Rogalski 
% 2022-06 

# The Linux command line for beginners 

# Run command line 
We use Git Bash for training 
to run: 
press Win button -> write git bash -> enter 

``` 
mkdir training 
cd training
```

# Where we are? 
``` 
pwd 
``` 
Print Working Directory

# List 
``` 
ls 
ls -al 
ls -1 
``` 

# Let's make directory 
``` 
mkdir directory1 
mkdir -p one/two/three 
``` 
# Let's move 
``` 
cd directory1 
pwd 
cd .. 
pwd 
``` 
Question 1 - where we will be after execution? 
``` 
cd . 
pwd 
``` 

# remove directory 
``` 
rm directory1 
rm one 
rm -r one 
``` 

# Lets print something 
``` 
echo "Hello World" 
``` 

# More 
https://devhints.io/bash 

# Some tools 

# Let's download book: 
curl https://www.gutenberg.org/files/135/135-0.txt > book.txt 

# Let's see content of text file 
``` 
cat book.txt 
``` 

# Let's find line contain ... 
``` 
grep string book.txt 
``` 

# Pipelines 
``` 
 env | grep COMPUTERNAME 
``` 

# Sort 
``` 
 cat book.txt | sort 
``` 

# firsts or lasts lines we are interested 
``` 
 cat book.txt | head 
 cat book.txt | tail -10 
``` 

# SED - stream editing (replace, remove text) 
``` 
cat book.txt | tail | sed 's/to/XXXX/g' 
``` 

# Lets count lines 
``` 
cat book.txt | wc -l 
``` 

# if you need help 
``` 
grep --help 
wc --help 
head --help 
``` 

# Scripts 
- crate file 'script.sh'  
- put in this file text: 
``` 
echo "Hello World" 
``` 
and save. 
- execute ./script.sh 

# Variables 
 Let's modify script.sh 
``` 
#!/bin/bash 
greeting="Welcome" 
user=$(whoami) 
day=$(date +%A) 
echo "$greeting back $user! Today is $day" 
``` 

# Calculate  
add.sh 
``` 
#!/bin/bash 
a=4; 
b=2; 
echo $[$a + $b] 
``` 

# IF

```
!/bin/bash

echo "Enter username"
read username
echo "Enter password"
read password

if [[ ( $username == "admin" && $password == "secret" ) ]]; then
echo "valid user"
else
echo "invalid user"
fi
```

# loop while

```
#!/bin/bash
valid=true
count=1
while [ $valid ]
do
echo $count
if [ $count -eq 5 ];
then
break
fi
((count++))
done
```
# echo loop

```
#!/bin/bash
for (( counter=10; counter>0; counter-- ))
do
echo -n "$counter "
done
printf "\n"
```

# read frop input

```
#!/bin/bash
echo "Enter Your Name"
read name
echo "Welcome $name"
```

# input parameter

```
#!/bin/bash
n=$1
if [ $n -lt 10 ];
then
echo "It is a one digit number"
else
echo "It is at least two digit number"
fi
```

# Function

```
#!/bin/bash
function Fun1()
{
echo 'I like bash programming'
}

Fun1
```

# read book in loop

```
#!/bin/bash
file='book.txt'
while read line; do
echo $line
done < $file
```
